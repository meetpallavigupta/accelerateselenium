package com.pallavi.sampletestproject.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


//Test Comment
public class TestBase {

	public static WebDriver driver;	
		
	public static void initialization(String exePath, String configUrl) {
		
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(configUrl);
		
		
		
		
	}
		
	}


