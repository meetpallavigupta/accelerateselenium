package com.pallavi.sampletestproject.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pallavi.sampletestproject.base.TestBase;
import com.pallavi.sampletestproject.config.TestConfig;
import com.pallavi.sampletestproject.pageobjects.LoginPageObjects;

public class LoginPageTestCases extends TestBase{
	
	LoginPageObjects loginPage;
	
	public LoginPageTestCases(){
    super();
   }
		
	@BeforeMethod
	public void setUp() {
   initialization("C:\\Users/lenovo/git/accelerateselenium/AccelerateSelenium/src/main/java/chromedriver.exe",TestConfig.url);
   
   
   loginPage = new LoginPageObjects();
	}
	
	@Test
	public void login() throws InterruptedException {
		
		loginPage.enterEmail("digispicetestmail@gmail.com");
		loginPage.clickNextButton();
		
		Thread.sleep(2000);
		
		loginPage.enterPassword("spice@1234");
		loginPage.clickNextButton();
		
		Thread.sleep(5000);
		String actual = driver.getCurrentUrl();
		String expected = "https://mail.google.com/mail/u/0/#inbox";

		Assert.assertEquals(actual, expected);
		
	}
}
