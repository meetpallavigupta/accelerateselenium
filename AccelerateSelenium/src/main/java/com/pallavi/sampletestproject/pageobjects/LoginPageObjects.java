package com.pallavi.sampletestproject.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.pallavi.sampletestproject.base.TestBase;

public class LoginPageObjects extends TestBase {


	
	@FindBy(id="identifierId")
	WebElement email;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(className="CwaK9")
	WebElement nextButton;
	
	 public LoginPageObjects(){

	        PageFactory.initElements(driver, this);

	    }
	 
	 public void enterEmail(String Email) {
		 
		 email.sendKeys(Email);
	 }

	 public void enterPassword(String Password) {
		 
		 password.sendKeys(Password);
	 }
	 
	 public void clickNextButton() {
		 
		 nextButton.click();
	 }
	
	
	
	
	
}
